let numbers = [1, 2],
    summ = 0;

while (numbers[1] < 4000000) {
    for (let i = 0; i < numbers.length; i++)
        summ += !(numbers[i] % 2) ? numbers[i] : 0;

    numbers = [numbers[1], numbers[0] + numbers[1]];
}

console.log(summ);