let summ = 0;

for (let i = 1; i <= 1000; i++)
    summ += (!(i % 3) || !(i % 5)) ? i : 0;

console.log(summ);